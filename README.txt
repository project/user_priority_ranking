User Priority Ranking

https://drupal.org/project/user_priority_ranking

Maintainer: iStryker (Tyler Struyk)
https://drupal.org/user/303676

Module Development Sponsored by the University of Waterloo

This module provides a function to sort a list of content based on priority the user gives them.   
This module pulls in functionality from multiple modules as well as its own.   

** More description to come ***     
