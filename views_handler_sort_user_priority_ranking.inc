<?php

/**
 * @file
 * Handler that supports sorting on flag priority. This works best if you use
 * sum aggeration. See README.txt for my information.
 */

class views_handler_sort_user_priority_ranking extends views_handler_sort {
  function option_definition() {
    $options = parent::option_definition();
    $options['priorityorder'] = array('default' => 'NONE');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['priorityorder'] = array(
      '#title' => t('Ordering of Priority Values'),
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->options['priorityorder'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->query->add_orderby(NULL, $this->table_alias . '.' . $this->real_field . ' IS NULL', $this->options['flagorder']);
    parent::query();
  }
}
