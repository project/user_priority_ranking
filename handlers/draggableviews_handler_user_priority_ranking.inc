<?php

/**
 * @file
 * User Priority Ranking implementation for draggableviews.
 */

$plugin = array(
  'label' => 'User Priority Ranking',
  'handler' => array(
    'class' => 'draggableviews_handler_user_priority_ranking',
  ),
);

/*
 * Implementation using the User Priority Ranking module.
 * This module should extends draggableviews_handler_flag_weights to use its get
 * function.
 */
class Draggableviews_handler_user_priority_ranking extends draggableviews_handler_flag_weights {

  function set($form_state) {
    global $user; // assume $extra['uid] = '***CURRENT_USER***'

    $fv = $form_state['values'];
    $view = $form_state['build_info']['args'][0];
    $relationship = $view->relationship['flag_content_rel'];
    $fid = $relationship->definition['extra'][0]['value'];
    $flag = flag_get_flag($relationship->options['flag']);

    // For global flags, use uid 0
    $uid = $flag->global ? 0 : $user->uid;

    // Save records to our flags table.
    foreach ($fv['draggableviews'] as $item) {
      // Make sure id is available.
      if (!isset($item['id'])) {
      continue;
      }
      //set the weight
      flag_weights_set_weight($fid, $flag->content_type, $item['id'], $uid, $item['weight']);

      //need to get limit as the the flag's name is not avaiable in the method below
      $limit = variable_get('flag_limit_' . $flag->name . '_value', FALSE);
      //set the priority
      user_priority_ranking_set_priority($fid, $flag->content_type, $item['id'], $uid, $item['weight'], $limit);
    }
  }
}
